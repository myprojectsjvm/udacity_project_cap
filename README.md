# CPPND: Capstone Snake Game Example

This is the improved project for the Capstone project in the [Udacity C++ Nanodegree Program](https://www.udacity.com/course/c-plus-plus-nanodegree--nd213). The code for this repo was inspired by [this](https://codereview.stackexchange.com/questions/212296/snake-game-in-c-with-sdl) excellent StackOverflow post and set of responses.

<img src="snake_game.gif"/>

The program structure was modified to use images instead of simple dots. The snake control was modified too in order to improve the performance and update just the necessary snake pieces.

In general, 4 main classes was created inspired in the [this](https://www.youtube.com/watch?v=iggmjJ_C_C4&list=PL1H1sBF1VAKXMz8kETLHRo1LwnvB08Q2J&index=2) Youtube playlist.

* sprite.h/cpp: main parent class and used to build basic blocks.
* piece.h/cpp: this class is child of sprite class and add image capability.
* food.h/cpp: this class is child of piece and is used to plot the food.
* body.h/cpp: this class is child of piece and is used to plot the snake itself.

The snake sprites were get from [link](https://digitalinnovation.one/artigos/colocando-sprites-no-jogo-da-cobrinha).

In the game 3 types of food can be found:

* Normal: Normal food that makes the snake grown up.
* Poison: Special food that makes the snake reverse.
* Cut: Special food that cut the snake by half.

## Rubrics

The project was modified to achieve mainly Object Oriented Programming criterias. The most important are:

* *The project demonstrates an understanding of C++ functions and control structures* Pointers access verification before use, for example, image load in *piece* class.
* *The project uses Object Oriented Programming techniques*: All classes above were created.
* *Classes use appropriate access specifiers for class members*: All classes above use appropriate access and the header files can be looked to more details.
* *Class constructors utilize member initialization lists*: All child classes above use initialization of parents and the source files can be looked to more details.
* *Classes abstract implementation details from their interfaces*: The *setImage* method from *Piece* class is an example. The user must pass just the image name.
* *Classes follow an appropriate inheritance hierarchy*: The structure described above was udes to achieve this criteria.

## Dependencies for Running Locally
* cmake >= 3.7
  * All OSes: [click here for installation instructions](https://cmake.org/install/)
* make >= 4.1 (Linux, Mac), 3.81 (Windows)
  * Linux: make is installed by default on most Linux distros
  * Mac: [install Xcode command line tools to get make](https://developer.apple.com/xcode/features/)
  * Windows: [Click here for installation instructions](http://gnuwin32.sourceforge.net/packages/make.htm)
* SDL2 >= 2.0
  * All installation instructions can be found [here](https://wiki.libsdl.org/Installation)
  >Note that for Linux, an `apt` or `apt-get` installation is preferred to building from source.
* SDL2_image>=2.0.0
  * All installation instructions can be found [here](https://www.libsdl.org/projects/SDL_image/)
  * For Ubunto users, the following command can be used: "sudo apt-get install libsdl2-image-dev".
* gcc/g++ >= 5.4
  * Linux: gcc / g++ is installed by default on most Linux distros
  * Mac: same deal as make - [install Xcode command line tools](https://developer.apple.com/xcode/features/)
  * Windows: recommend using [MinGW](http://www.mingw.org/)

## Basic Build Instructions

1. Clone this repo.
2. Make a build directory in the top level directory: `mkdir build && cd build`
3. Compile: `cmake .. && make`
4. Run it: `./SnakeGame`.


## CC Attribution-ShareAlike 4.0 International


Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
