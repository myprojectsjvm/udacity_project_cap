#include "piece.h"
#include "SDL2/SDL_image.h"
#include <iostream>

/**
 * @brief   Construct a new Piece object.
 * 
 * @param   color Color defined by @ref SDL_MapRGB.
 * @param   x     X cordinate.
 * @param   y     Y cordinate.
 * @param   w     Width.
 * @param   h     Height.
 */
Piece::Piece(Uint32 color, int x, int y, int w, int h) :
Sprite(color, x, y, w, h)
{

}

/**
 * @brief Useful routine to load and image.
 * 
 * @param filename Image name.
 */
void Piece::setImage(const char *filename) {
    if (filename != NULL) {
        SDL_Surface *loadedImage = nullptr;
        loadedImage = IMG_Load(filename);
        if (loadedImage != nullptr) {
            _surface = loadedImage;
            int xOld = _rect.x;
            int yOld = _rect.y;
            _rect = _surface->clip_rect;
            _rect.x = xOld;
            _rect.y = yOld;
        }
    }
    else {
        std::cerr << "Image could not be created.\n";
        std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
    }
}