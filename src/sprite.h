#ifndef SPRITE_H
#define SPRITE_H

#include "SDL.h"

class Sprite {

    public:

        Sprite(Uint32 color, int x, int y, int w, int h);
        SDL_Rect *getRectPtr(void);
        SDL_Surface *getSurfacePtr(void);
        void setXY(int x, int y);

    protected:

        SDL_Surface *_surface;
		SDL_Rect _rect;

};

#endif