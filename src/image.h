#ifndef IMAGE_H
#define IMAGE_H

namespace Image {
    const char kImageFoodNormal[]           {"../snake/Food_Normal.png"};
    const char kImageFoodReverse[]          {"../snake/Food_Reverse.png"};
    const char kImageFoodCut[]              {"../snake/Food_Cut.png"};
    const char kSnakeHeadUp[]               {"../snake/Snake_Head_Up.png"};
    const char kSnakeHeadDown[]             {"../snake/Snake_Head_Down.png"};
    const char kSnakeHeadRight[]            {"../snake/Snake_Head_Right.png"};
    const char kSnakeHeadLeft[]             {"../snake/Snake_Head_Left.png"};
    const char kSnakeTailUp[]               {"../snake/Snake_Tail_Up.png"};
    const char kSnakeTailDown[]             {"../snake/Snake_Tail_Down.png"};
    const char kSnakeTailRight[]            {"../snake/Snake_Tail_Right.png"};
    const char kSnakeTailLeft[]             {"../snake/Snake_Tail_Left.png"};
    const char kSnakeBodyNormalUp[]         {"../snake/Snake_Body_Normal_Vertical.png"};
    const char kSnakeBodyNormalDown[]       {"../snake/Snake_Body_Normal_Vertical.png"};
    const char kSnakeBodyNormalRight[]      {"../snake/Snake_Body_Normal_Horizontal.png"};
    const char kSnakeBodyNormalLeft[]       {"../snake/Snake_Body_Normal_Horizontal.png"};
    const char kSnakeBodyCurvedUpRight[]    {"../snake/Snake_Body_Curved_Up_Right.png"};
    const char kSnakeBodyCurvedUpLeft[]     {"../snake/Snake_Body_Curved_Up_Left.png"};
    const char kSnakeBodyCurvedDownRight[]  {"../snake/Snake_Body_Curved_Down_Right.png"};
    const char kSnakeBodyCurvedDownLeft[]   {"../snake/Snake_Body_Curved_Down_Left.png"};
}

#endif