#ifndef FOOD_H
#define FOOD_H

#include "piece.h"

class Food : public Piece {

    public:

        enum class Type {
            kNormal,    // Normal food.
            kReverse,   // Poison the snake and make it to reverse.
            kCut,       // Poison the snake and make by half.
            kNone,      // Useful.
        };

        Food(Type type, Uint32 color, int x, int y, int w, int h);

        void update(void);

        void updateImage(void);

        Type getType(void);

        int x; // X cordinate;
        int y; // Y cordinate;

    protected:

        int _w; // Width.
        int _h; // Height.
        Type _type;

};

#endif