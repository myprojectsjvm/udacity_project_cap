#ifndef SNAKE_H
#define SNAKE_H

#include <vector>
#include "SDL.h"
#include "body.h"
#include "food.h"

class Snake {
 public:
  const int kCutTh = 6; // Snake size must be greater or equal than 6 to cut.

  Snake(const std::size_t screen_width, const std::size_t screen_height,
    const std::size_t grid_width, const std::size_t grid_height);

  void Update();

  void FoodEvent(Food::Type event);
  bool SnakeCell(int x, int y);

  Body::Direction direction = Body::Direction::kUp;

  float speed{0.06f};
  int size{3};
  bool alive{true};
  float head_x;
  float head_y;
  std::vector<Body> body;

 private:
  void UpdateHead();
  void UpdateBodyGrown(int currentX, int currentY,
    int prevX, int prevY, Body::Direction dir);
  void UpdateBodyMove(int currentX, int currentY,
    int prevX, int prevY, Body::Direction dir);
  void UpdateBodyReverse(int currentX, int currentY,
    int prevX, int prevY, Body::Direction dir);
  void UpdateBodyCut(int currentX, int currentY,
    int prevX, int prevY, Body::Direction dir);
  void UpdateBody(int currentX, int currentY,
    int prevX, int prevY, Body::Direction dir);

  Food::Type foodEvent{Food::Type::kNone};
  int screen_width;
  int screen_height;
  int grid_width;
  int grid_height;

};

#endif