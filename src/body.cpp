#include "body.h"

/**
 * @brief   Construct a new Body object.
 * 
 * @param   color Color defined by @ref SDL_MapRGB.
 * @param   x     X cordinate.
 * @param   y     Y cordinate.
 * @param   w     Width.
 * @param   h     Height.
 */

Body::Body(Type type, Direction direction, Uint32 color, int x, int y, int w, int h) :
Piece(color, x, y, w, h)
{
    _type = type;
    _direction = direction;
    _w = w;
    _h = h;
    this->x = x;
    this->y = y;
    updateImage();
}

/**
 * @brief X and Y update.
 */
void Body::update() {
    setXY(x*_w, y*_h);
}

/**
 * @brief Set the Type object.
 * 
 * @param type New direction.
 */
void Body::setType(Type type) {
    _type = type;
}

/**
 * @brief Set the Direction object.
 * 
 * @param direction New direction. 
 */
void Body::setDirection(Direction direction) {
    _previousDirection = _direction;
    _direction = direction;
}

/**
 * @brief Set the Direction object.
 * 
 * @param direction New direction. 
 */
void Body::setInverse(void) {

    if (_type == Type::kBodyCurved) {
        Direction temp = getOposite(_direction);
        _direction = getOposite(_previousDirection);
        _previousDirection = temp;
    }
    else {
        _direction = getOposite(_direction);
    }

}

/**
 * @brief Set the Body object image.
 * 
 * @todo Improve image get.
 */
void Body::updateImage(void) {
    if (_type == Type::kHead) {
        if (_direction == Direction::kUp) {
            setImage(Image::kSnakeHeadUp);
        }
        else if (_direction == Direction::kDown) {
            setImage(Image::kSnakeHeadDown);
        }
        else if (_direction == Direction::kRight) {
            setImage(Image::kSnakeHeadRight);
        }
        else if (_direction == Direction::kLeft) {
            setImage(Image::kSnakeHeadLeft);
        }
    }
    else if (_type == Type::kTail) {
        if (_direction == Direction::kUp) {
            setImage(Image::kSnakeTailUp);
        }
        else if (_direction == Direction::kDown) {
            setImage(Image::kSnakeTailDown);
        }
        else if (_direction == Direction::kRight) {
            setImage(Image::kSnakeTailRight);
        }
        else if (_direction == Direction::kLeft) {
            setImage(Image::kSnakeTailLeft);
        }
    }
    else if (_type == Type::kBodyNormal) {
        if (_direction == Direction::kUp) {
            setImage(Image::kSnakeBodyNormalUp);
        }
        else if (_direction == Direction::kDown) {
            setImage(Image::kSnakeBodyNormalDown);
        }
        else if (_direction == Direction::kRight) {
            setImage(Image::kSnakeBodyNormalRight);
        }
        else if (_direction == Direction::kLeft) {
            setImage(Image::kSnakeBodyNormalLeft);
        }
    }
    else if (_type == Type::kBodyCurved) {
        if ((_direction == Direction::kUp && _previousDirection == Direction::kLeft) ||
            (_direction == Direction::kRight && _previousDirection == Direction::kDown)
        ) {
            setImage(Image::kSnakeBodyCurvedUpRight);
        }
        else if ((_direction == Direction::kUp && _previousDirection == Direction::kRight) ||
            (_direction == Direction::kLeft && _previousDirection == Direction::kDown)
        ) {
            setImage(Image::kSnakeBodyCurvedUpLeft);
        }
        else if ((_direction == Direction::kDown && _previousDirection == Direction::kLeft) ||
            (_direction == Direction::kRight && _previousDirection == Direction::kUp)
        ) {
            setImage(Image::kSnakeBodyCurvedDownRight);
        }
        else if ((_direction == Direction::kDown && _previousDirection == Direction::kRight) ||
            (_direction == Direction::kLeft && _previousDirection == Direction::kUp)
        ) {
            setImage(Image::kSnakeBodyCurvedDownLeft);
        }
    }
}

/**
 * @brief Getter to object type.
 * 
 * @return Body type. 
 */
Body::Type Body::getType(void) {
    return _type;
}

/**
 * @brief Getter to object direcion.
 * 
 * @return Body direction. 
 */
Body::Direction Body::getDirection(void) {
    return _direction;
}

/**
 * @brief Get the oposite direction
 * 
 * @param   direction Current direction.
 * @return  Body::Direction 
 */
Body::Direction Body::getOposite(Body::Direction direction) {
    if (direction == Direction::kUp) {
        return Direction::kDown;
    }
    else if (direction == Direction::kDown) {
        return Direction::kUp;
    }
    else if (direction == Direction::kLeft) {
        return Direction::kRight;
    }
    else if (direction == Direction::kRight) {
        return Direction::kLeft;
    }
}
