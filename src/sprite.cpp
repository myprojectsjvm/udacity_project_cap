#include "sprite.h"
#include <iostream>

/**
 * @brief   Construct a new Sprite object.
 * 
 * @param   color Color defined by @ref SDL_MapRGB.
 * @param   x     X cordinate.
 * @param   y     Y cordinate.
 * @param   w     Width.
 * @param   h     Height.
 */
Sprite::Sprite(Uint32 color, int x, int y, int w, int h) {

    _surface = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0 , 0);

    if (nullptr == _surface) {
        std::cerr << "Surface could not be created.\n";
        std::cerr << " SDL_Error: " << SDL_GetError() << "\n";
    }
    else {

        SDL_FillRect(_surface, NULL, color);

        _rect = _surface->clip_rect;

        _rect.x = x;
        _rect.y = y;

    }

}

/**
 * @brief   Useful getter to get the retangle pointer.
 * 
 * @return  SDL_Rect* Retangle pointer.
 */
SDL_Rect *Sprite::getRectPtr(void) {
    return &_rect;
}

/**
 * @brief   Useful getter to get the surface pointer.
 * 
 * @return  SDL_Surface* Surface pointer.
 */
SDL_Surface *Sprite::getSurfacePtr(void) {
    return _surface;
}

/**
 * @brief  Set X and Y cordinates.
 * 
 * @param  x  X cordinate.
 * @param  y  Y cordinate.
 */
void Sprite::setXY(int x, int y) {

    _rect.x = x;
    _rect.y = y;

}
