#include "snake.h"
#include <cmath>
#include <iostream>
#include <algorithm>

Snake::Snake(const std::size_t screen_width, const std::size_t screen_height,
  const std::size_t grid_width, const std::size_t grid_height) :
    screen_width(screen_width),
    screen_height(screen_height),
    grid_width(grid_width),
    grid_height(grid_height),
    head_x(grid_width / 2),
    head_y(grid_height / 2)
{
  int x = static_cast<int>(head_x);
  int y = static_cast<int>(head_y);
  // First pieces:
  Body snakeTail(
    Body::Type::kTail, Body::Direction::kUp,
    0xFFFFFFFF, x, y+2, screen_width/grid_width, screen_height/grid_height
  );
  Body snakeBody(
    Body::Type::kBodyNormal, Body::Direction::kUp,
    0xFFFFFFFF, x, y+1, screen_width/grid_width, screen_height/grid_height
  );
  Body snakeHead(
    Body::Type::kHead, Body::Direction::kUp,
    0xFFFFFFFF, x, y, screen_width/grid_width, screen_height/grid_height
  );
  body.emplace_back(snakeTail);
  body.emplace_back(snakeBody);
  body.emplace_back(snakeHead);
}

void Snake::Update() {
  // We first capture the head's cell before updating.
  int prevX = static_cast<int>(head_x);
  int prevY = static_cast<int>(head_y);
  UpdateHead();
  // Capture the head's cell after updating.
  int currentX = static_cast<int>(head_x);
  int currentY = static_cast<int>(head_y);
  // Update all of the body vector items if the snake head has moved to a new
  // cell.
  if (currentX != prevX || currentY != prevY) {
    UpdateBody(currentX, currentY, prevX, prevY, direction);
  }
}

void Snake::UpdateHead() {
  switch (direction) {
    case Body::Direction::kUp:
      head_y -= speed;
      break;

    case Body::Direction::kDown:
      head_y += speed;
      break;

    case Body::Direction::kLeft:
      head_x -= speed;
      break;

    case Body::Direction::kRight:
      head_x += speed;
      break;
  }

  // Wrap the Snake around to the beginning if going off of the screen.
  head_x = fmod(head_x + grid_width, grid_width);
  head_y = fmod(head_y + grid_height, grid_height);
}

void Snake::UpdateBodyMove(int currentX, int currentY,
  int prevX, int prevY, Body::Direction dir)
{
  // Put tail in the head position:
  std::rotate(body.begin(), body.begin() + 1, body.end());

  // Tail is the new head:
  body[body.size()-1].x = currentX;
  body[body.size()-1].y = currentY;
  body[body.size()-1].setType(Body::Type::kHead);
  body[body.size()-1].setDirection(dir);
  body[body.size()-1].updateImage();

  // Previous head is a body now:
  body[body.size()-2].setDirection(dir);
  if (body[body.size()-1].getDirection() == body[body.size()-3].getDirection()) {
    body[body.size()-2].setType(Body::Type::kBodyNormal);
  }
  else {
    body[body.size()-2].setType(Body::Type::kBodyCurved);
  }
  body[body.size()-2].updateImage();

  // Tail update:
  body[0].setType(Body::Type::kTail);
  body[0].updateImage();
}

void Snake::UpdateBodyGrown(int currentX, int currentY,
  int prevX, int prevY, Body::Direction dir)
{
  // First pieces:
  Body snakeHead(
    Body::Type::kHead, dir,
    0xFFFFFFFF, currentX, currentY, screen_width/grid_width, screen_height/grid_height
  );
  body.emplace_back(snakeHead);
  size++;

  // Previous head is a body now:
  body[body.size()-2].setDirection(dir);
  if (body[body.size()-1].getDirection() == body[body.size()-3].getDirection()) {
    body[body.size()-2].setType(Body::Type::kBodyNormal);
  }
  else {
    body[body.size()-2].setType(Body::Type::kBodyCurved);
  }
  body[body.size()-2].updateImage();
}

void Snake::UpdateBodyReverse(int currentX, int currentY,
  int prevX, int prevY, Body::Direction dir)
{
  std::reverse(body.begin(),body.end());
  for (Body &b : body) {
    b.setInverse();
  }
  // Head update:
  body[body.size()-1].setType(Body::Type::kHead);
  body[body.size()-1].updateImage();
  // Tail update:
  body[0].setType(Body::Type::kTail);
  body[0].updateImage();
  // Head position update:
  head_x = static_cast<float>(body[body.size()-1].x);
  head_y = static_cast<float>(body[body.size()-1].y);
  direction = body[body.size()-1].getDirection();
}

void Snake::UpdateBodyCut(int currentX, int currentY,
  int prevX, int prevY, Body::Direction dir)
{
  if (body.size() >= kCutTh) {
    // Cut:
    int cut = body.size()/2;
    for (int i = 0; i < cut; i++) {
      body.erase(body.begin());
    }
    // Tail update:
    body[0].setType(Body::Type::kTail);
    body[0].updateImage();
    // Head position update:
    head_x = static_cast<float>(body[body.size()-1].x);
    head_y = static_cast<float>(body[body.size()-1].y);
    direction = body[body.size()-1].getDirection();
  }
}

void Snake::UpdateBody(int currentX, int currentY,
  int prevX, int prevY, Body::Direction dir)
{
  if (foodEvent == Food::Type::kNone) {
    UpdateBodyMove(currentX, currentY, prevX, prevY, dir);
  }
  else if (foodEvent == Food::Type::kReverse) {
     UpdateBodyReverse(currentX, currentY, prevX, prevY, dir);
  }
  else if (foodEvent == Food::Type::kCut) {
     UpdateBodyCut(currentX, currentY, prevX, prevY, dir);
  }
  else {
    UpdateBodyGrown(currentX, currentY, prevX, prevY, dir);
  }

  foodEvent = Food::Type::kNone;

  // Check if the snake has died.
  for (auto &item : body) {
    if (currentX == item.x && currentY == item.y &&
      item.getType() != Body::Type::kHead)
    {
      alive = false;
    }
  }
}

void Snake::FoodEvent(Food::Type event) { foodEvent = event; }

// Inefficient method to check if cell is occupied by snake.
bool Snake::SnakeCell(int x, int y) {
  if (x == static_cast<int>(head_x) && y == static_cast<int>(head_y)) {
    return true;
  }
  for (auto const &item : body) {
    if (x == item.x && y == item.y) {
      return true;
    }
  }
  return false;
}