#include "renderer.h"
#include <iostream>
#include <string>

Renderer::Renderer(const std::size_t screen_width,
                   const std::size_t screen_height,
                   const std::size_t grid_width, const std::size_t grid_height)
    : screen_width(screen_width),
      screen_height(screen_height),
      grid_width(grid_width),
      grid_height(grid_height) {
  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cerr << "SDL could not initialize.\n";
    std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
  }

  // Create Window
  sdl_window = SDL_CreateWindow("Snake Game", SDL_WINDOWPOS_CENTERED,
                                SDL_WINDOWPOS_CENTERED, screen_width,
                                screen_height, SDL_WINDOW_SHOWN);

  if (nullptr == sdl_window) {
    std::cerr << "Window could not be created.\n";
    std::cerr << " SDL_Error: " << SDL_GetError() << "\n";
  }

  // Create renderer
  sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED);
  if (nullptr == sdl_renderer) {
    std::cerr << "Renderer could not be created.\n";
    std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
  }
}

Renderer::~Renderer() {
  SDL_DestroyWindow(sdl_window);
  SDL_Quit();
}

void Renderer::Render(Snake &snake, std::vector<Food> &food) {
  SDL_Texture *tex;
  
  // Clear screen
  SDL_SetRenderDrawColor(sdl_renderer, 0x1E, 0x1E, 0x1E, 0xFF);
  SDL_RenderClear(sdl_renderer);

  /// Render food
  for (Food &currentFood : food) {
    currentFood.update();
    tex = SDL_CreateTextureFromSurface(sdl_renderer, currentFood.getSurfacePtr());
    if (nullptr == tex) {
      std::cerr << "Texture could not be created.\n";
      std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
    }
    else {
      SDL_RenderCopy(sdl_renderer, tex, NULL, currentFood.getRectPtr());
    }
  }

  // Render snake's body
  for (Body &point : snake.body) {
    point.update();
    tex = SDL_CreateTextureFromSurface(sdl_renderer, point.getSurfacePtr());
    if (nullptr == tex) {
      std::cerr << "Texture could not be created.\n";
      std::cerr << "SDL_Error: " << SDL_GetError() << "\n";
    }
    else {
      SDL_RenderCopy(sdl_renderer, tex, NULL, point.getRectPtr());
    }
  }

  // Update Screen
  SDL_RenderPresent(sdl_renderer);
}

void Renderer::UpdateWindowTitle(int score, int fps) {
  std::string title{"Snake Score: " + std::to_string(score) + " FPS: " + std::to_string(fps)};
  SDL_SetWindowTitle(sdl_window, title.c_str());
}
