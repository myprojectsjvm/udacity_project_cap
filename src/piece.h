#ifndef PIECE_H
#define PIECE_H

#include "sprite.h"

class Piece : public Sprite {

    public:

        Piece(Uint32 color, int x, int y, int w, int h);
        void setImage(const char *filename);

    protected:

};

#endif