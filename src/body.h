#ifndef BODY_H
#define BODY_H

#include "piece.h"
#include "image.h"

class Body : public Piece {

    public:

        enum class Type { kHead, kBodyNormal, kBodyCurved, kTail };
        enum class Direction { kUp, kDown, kLeft, kRight };

        Body():
            Piece(Uint32(0xFF0000FF), 0, 0, 20, 20),
            _w(20), _h(20){}
        Body(int x, int y):
            Piece(Uint32(0xFF0000FF), 0, 0, 20, 20),
            x(x), y(y),
            _w(20), _h(20){}
        Body(Type type, Direction direction, Uint32 color, int x, int y, int w, int h);

        void update(void);

        void setType(Type type);
        void setDirection(Direction direction);
        void setInverse();
        void updateImage(void);

        Type getType(void);
        Direction getDirection(void); 

        int x; // X cordinate;
        int y; // Y cordinate;

    protected:

        Direction getOposite(Direction direction);

        int _w; // Width.
        int _h; // Height.
        Type _type;
        Direction _direction;
        Direction _previousDirection;

};

#endif