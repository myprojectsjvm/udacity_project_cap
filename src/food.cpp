#include "food.h"
#include "image.h"

/**
 * @brief   Construct a new Food object.
 * 
 * @param   color Color defined by @ref SDL_MapRGB.
 * @param   x     X cordinate.
 * @param   y     Y cordinate.
 * @param   w     Width.
 * @param   h     Height.
 */

Food::Food(Type type, Uint32 color, int x, int y, int w, int h) :
Piece(color, x, y, w, h)
{
    _w = w;
    _h = h;
    _type = type;
    updateImage();
}

/**
 * @brief X and Y update.
 */
void Food::update() {
    setXY(x*_w, y*_h);
}

/**
 * @brief Set the Food object image.
 */
void Food::updateImage(void) {

    if (_type == Type::kNormal) {
        setImage(Image::kImageFoodNormal);
    }
    else if (_type == Type::kReverse) {
        setImage(Image::kImageFoodReverse);
    }
    else if (_type == Type::kCut) {
        setImage(Image::kImageFoodCut);
    }
            
}

/**
 * @brief Getter to object type.
 * 
 * @return Body type. 
 */
Food::Type Food::getType(void) {
    return _type;
}